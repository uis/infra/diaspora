import re
import yaml
from six import string_types

from collections import OrderedDict

def setify(x):
    if isinstance(x, string_types):
        return { x }
    return set(x)

def boxes():
    with open("boxes.yaml") as boxesfile:
        boxes = yaml.safe_load(boxesfile)
    for (name, info) in boxes.items():
        info.setdefault('need', { })
        info.setdefault('provide', { })
        if 'need-power' in info:
            m = re.match(r"(\d+)\+(\d+)(?:\@(\d+(?:\.\d+)?)A?)?$",
                         info['need-power'])
            if m:
                oneeded = int(m.group(1))
                ospare = int(m.group(2))
                if 'to' in info and info['to'].startswith("wcdc-"):
                    colours = ['k', 'u', 'none']
                else:
                    colours = ['o', 'k', 'u']
                def outlets(n): return 'outlets-' + colours[n]
                def current(n): return 'current-' + colours[n]
                                        
                if m.group(3) == None:
                    needcurrent = None
                else:
                    needcurrent = float(m.group(3))
                if ospare == 0:
                    info['need'][outlets(2)] = oneeded
                    if needcurrent != None: info['need'][current(2)] = needcurrent
                elif oneeded == 2 * ospare:
                    info['need'][outlets(2)] = info['need'][outlets(0)] = \
                                                info['need'][outlets(1)] = \
                                                ospare
                    if needcurrent != None:
                        info['need'][current(2)] = \
                                                  info['need'][current(0)] = \
                                                  info['need'][current(1)] = \
                                                  needcurrent
                elif oneeded == ospare:
                    info['need'][outlets(0)] = info['need'][outlets(1)] = \
                                                ospare
                    if needcurrent != None:
                        info['need'][current(0)] = \
                                                  info['need'][current(1)] = \
                                                  needcurrent
                else:
                    raise Exception("Unsupported need-power spec", info['need-power'])
            else:
                raise Exception("Invalid need-power spec", info['need-power'])
        if 'net' in info:
            for (port) in info['net']:
                net = str(info['net'][port])
                info['net'][port] = net
                tag = 'net-' + net
                info['need'][tag] = info['need'].get(tag, 0) + 1
        else: info['net'] = { }
        if 'provide-net' in info:
            for (net, portlist) in info['provide-net'].items():
                for portrange in portlist.split(","):
                    m = re.match(r"(\d+)(?:-(\d+))?$", portrange)
                    if not m:
                        raise Exception("Unparseable port list", portlist)
                    if m.group(2) != None:
                        ports = range(int(m.group(1)), int(m.group(2)) + 1)
                    else:
                        ports = [int(m.group(1))]
                    for port in ports:
                        net = str(net)
                        tag = 'net-' + net
                        info['provide'][tag] = info['provide'].get(tag, 0) + 1
        for rel in ('different-group', 'different-room', 'same-rack'):
            info[rel] = setify(info.get(rel, set()))
            # Make sure these relations are symmetric
            for other in [boxes[x] for x in info[rel]]:
                other[rel] = setify(other.get(rel, set()))
                other[rel].add(name)
    return boxes

def places():
    with open("places.yaml") as placesfile:
        places = yaml.safe_load(placesfile)
        for (name, info) in places.items():
            if 'room' not in info:
                info['room'] = name.partition('-')[0]
            if 'room' in info and 'group' not in info:
                info['group'] = name
    return places

def summarize_power(box):
    if 'to' in box and box['to'].startswith("wcdc-"):
        colours = ['k', 'u', 'none']
    else:
        colours = ['o', 'k', 'u']
    def outlets(n): 'outlets-' + colours[n]
    def current(n): 'current-' + colours[n]
        
    need = box['need']
    if (need.get(outlets(0), 0) == need.get(outlets(1), 0) and
        need.get(outlets(2), 0) == 0):
        outletstr = ("%d+%d" %
                     (need.get(outlets(0), 0), need.get(outlets(1), 0)))
        if (current(0) in need and current(1) in need and
            need[current(0)] == need[current(1)]):
            return "%s@%.1fA" % (outletstr, need[current(0)])
        if current(0) not in need and current(1) not in need:
            return outletstr
    if (need.get(outlets(0), 0) == 0 and need.get(outlets(1), 0) == 0):
        outletstr = "%d+0" % (need.get(outlets(2), 0),)
        if current(2) in need:
            return "%s+0@%.1fA" % (outletstr, need[current(2)])
        else:
            return outletstr
    if (need.get(outlets(0), 0) == need.get(outlets(1), 0) and
        need.get(outlets(0), 0) == need.get(outlets(2), 0)):
        outletstr = "%d+%d" % (need.get(outlets(0), 0) +
                               need.get(outlets(1), 0),
                               need.get(outlets(2), 0))
        if (current(0) in need and current(1) in need and
            current(2) in need and
            need[current(0)] == need[current(1)] and
            need[current(0)] == need[current(2)]):
            return "%s@%.1fA" % (outletstr, need[current(0)])
        if (current(0) not in need and current(1) not in need and
            current(2) not in need):
            return outletstr
    raise Exception("Unsupported power needs", need)

def schedule():
    with open("schedule.yaml") as schedfile:
        schedule = OrderedDict(yaml.safe_load(schedfile))
    return schedule
